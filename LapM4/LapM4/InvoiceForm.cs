﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LapM4
{
    public partial class InvoiceForm : Form
    {
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-FTC9FLG;Initial Catalog=lapM4;Integrated Security=True; " +
            "MultipleActiveResultSets = True;");
        SqlCommand command;
        string sql;
        string StaffID;
        string StaffName;
        double UnitPrice;
        DateTime thisDay = DateTime.Today;
        public InvoiceForm()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btLogOut_Click(object sender, EventArgs e)
        {
            LoginForm.LoginAccountID = null;
            LoginForm ss = new LoginForm();
            ss.Show();
            this.Hide();
        }

        private void InvoiceForm_Load(object sender, EventArgs e)
        {
            if (LoginForm.LoginAccountType == "User")
            {
                btStaffs.Visible = false;
                btAccounts.Visible = false;
            }
            StaffID = LoginForm.LoginStaffID.ToString();
            try
            {
                con.Open();
                sql = "SELECT StaffID FROM Staff WHERE StaffID = @p1";
                command = new SqlCommand(sql, con);
                command.Parameters.AddWithValue("@p1", StaffID);
                //command.Parameters.AddWithValue("@p2", StaffName);
                StaffName = command.ExecuteScalar().ToString();

                con.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            StaffID = LoginForm.LoginStaffID.ToString();
            try
            {
                con.Open();
                sql = "SELECT StaffName FROM Staff WHERE StaffID = @p1";
                command = new SqlCommand(sql, con);
                command.Parameters.AddWithValue("@p1", StaffID);
                StaffName = command.ExecuteScalar().ToString();

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            txtStaffID.Text = StaffID;
            txtStaffName.Text = StaffName;
            txtDate.Text = thisDay.ToString("d");
            txtQuantity.Text = "1";
        }

        private void txtStaffID_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCustomerID_TextChanged(object sender, EventArgs e)
        {
            txtCustomerName.Text = "";
            try
            {
                con.Open();
                sql = "SELECT CustomerName FROM Customer WHERE CustomerID = @p1";
                command = new SqlCommand(sql, con);
                command.Parameters.AddWithValue("@p1", txtCustomerID.Text);
              
                if( command.ExecuteScalar() != null)
                {
                    txtCustomerName.Text = command.ExecuteScalar().ToString();
                }
                con.Close();
            }
            catch(Exception ex)
            {

            }
           
        }

        private void txtProductID_TextChanged(object sender, EventArgs e)
        {
            txtProductName.Text = "";
            txtQuantity.Text = "1";
            try
            {
                con.Open();
                sql = "SELECT * FROM Product WHERE ProductID = @p1";
                command = new SqlCommand(sql, con);
                command.Parameters.AddWithValue("@p1", txtProductID.Text);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    txtProductName.Text = reader["ProductName"].ToString();
                    UnitPrice = Convert.ToDouble(reader["UnitPrice"]);
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btProducts_Click(object sender, EventArgs e)
        {
            ProductForm pf = new ProductForm();
            pf.Show();
            this.Hide();
        }

        private void btCustomers_Click(object sender, EventArgs e)
        {
            CustomerForm cf = new CustomerForm();
            cf.Show();
            this.Hide();
        }

        private void btStaffs_Click(object sender, EventArgs e)
        {
            StaffForm sf = new StaffForm();
            sf.Show();
            this.Hide();
        }

        private void btAccounts_Click(object sender, EventArgs e)
        {
            AccountForm af = new AccountForm();
            af.Show();
            this.Hide();
        }

        private void btIncrease_Click(object sender, EventArgs e)
        {
            txtQuantity.Text = (Convert.ToInt32(txtQuantity.Text) + 1).ToString();
        }

        private void btDecrease_Click(object sender, EventArgs e)
        {
            if(txtQuantity.Text != "1")
            {
                txtQuantity.Text = (Convert.ToInt32(txtQuantity.Text) - 1).ToString();
            }
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            if(txtProductID.Text != "" && txtProductName.Text != "")
            {
                double Amount = Convert.ToDouble(txtQuantity.Text) * UnitPrice;
                string[] row = new string[]
                {
                    txtProductID.Text,
                    txtProductName.Text,
                    txtQuantity.Text,
                    UnitPrice.ToString("F"),
                    Amount.ToString("F")
                };
                dataGridView1.Rows.Add(row);
                if(txtTotal.Text == "")
                {
                    txtTotal.Text = Amount.ToString("F");
                }
                else
                {
                    txtTotal.Text = (Convert.ToDouble(txtTotal.Text) + Amount).ToString("F");
                }
                txtProductID.Text = "";
                txtProductName.Text = "";
                txtQuantity.Text = "1";
            }
        }

        private void btRemove_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = dataGridView1.SelectedRows[0];
            double Amount = Convert.ToDouble(row.Cells["Amount"].Value);
            txtTotal.Text = (Convert.ToDouble(txtTotal.Text) - Amount).ToString();
            dataGridView1.Rows.RemoveAt(row.Index);
        }

        private void btCreate_Click(object sender, EventArgs e)
        {
            if(dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show("Invalid: no product.");
            }
            else if(txtCustomerID.Text == "" || txtCustomerName.Text == "")
            {
                MessageBox.Show("Please fill in customer information.");
            }
            else
            {
                try
                {
                    con.Open();
                    sql = "INSERT INTO Invoice VALUES (@p1, @p2, @p3, @p4)";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue("@p1", txtDate.Text);
                    command.Parameters.AddWithValue("@p2", txtCustomerID.Text);
                    command.Parameters.AddWithValue("@p3", txtStaffID.Text);
                    command.Parameters.AddWithValue("@p4", txtTotal.Text);
                    command.ExecuteNonQuery();

                    sql = "SELECT IDENT_CURRENT('Invoice')";
                    command = new SqlCommand(sql, con);
                    int InvoiceID = Convert.ToInt32(command.ExecuteScalar());
                    for(int i=0; i<dataGridView1.Rows.Count; i++)
                    {
                        int ProductID = Convert.ToInt32(dataGridView1.Rows[i].Cells["ProductID"].Value);
                        double Quantity = Convert.ToInt32(dataGridView1.Rows[i].Cells["Quantity"].Value);
                        double SellingPrice = Convert.ToDouble(dataGridView1.Rows[i].Cells["Quantity"].Value);
                        
                        sql = "INSERT INTO InvoiceDetail VALUES(@p1, @p2, @p3, @p4)";
                        command = new SqlCommand(sql, con);
                        command.Parameters.AddWithValue("@p1", InvoiceID);
                        command.Parameters.AddWithValue("@p2", ProductID);
                        command.Parameters.AddWithValue("@p3", Quantity);
                        command.Parameters.AddWithValue("@p4", SellingPrice);
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                    MessageBox.Show("Created successfully.");
                    InvoiceForm_Load(this, new EventArgs());
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void btClear_Click(object sender, EventArgs e)
        {
            txtDate.Text = thisDay.ToString("d");
            txtSearch.Text = "";
            txtProductName.Text = "";
            txtProductID.Text = "";
            txtQuantity.Text = "1";
            dataGridView1.Rows.Clear();
            txtTotal.Text = "";
            txtCustomerID.Text = "";
            txtCustomerName.Text = "";
            txtStaffID.Text = LoginForm.LoginStaffID.ToString();
            txtStaffName.Text = StaffName;
        }

        private void btSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == "")
            {
                MessageBox.Show("Please enter an invoice ID to search.");
            }
            else
            {
                string invoiceSearhID = txtSearch.Text;
                btClear_Click(this, new EventArgs());
                txtSearch.Text = invoiceSearhID;
                try
                {
                    con.Open();
                    sql = "SELECT InvoiceID FROM Invoice WHERE InvoiceID = @p1";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue("@p1", txtSearch.Text);

                    if(command.ExecuteScalar() == null)
                    {
                        MessageBox.Show("Search not found.");
                    }
                    else
                    {
                        sql = "SELECT * FROM Invoice WHERE InvoiceID = " + txtSearch.Text;
                        command = new SqlCommand(sql, con);
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            txtDate.Text = Convert.ToDateTime(reader["InvoiceDate"]).ToString("d");
                            txtCustomerID.Text = reader["CustomerID"].ToString();
                            txtStaffID.Text = reader["StaffID"].ToString();
                            txtTotal.Text = Convert.ToDouble(reader["Total"]).ToString("F");
                        }
                        sql = "SELECT CustomerName FROM Customer WHERE CustomerID = " + txtCustomerID.Text;
                        command = new SqlCommand(sql, con);
                        txtCustomerName.Text = command.ExecuteScalar().ToString();

                        sql = "SELECT StaffName FROM Staff WHERE StaffID = " + txtStaffID.Text;
                        command = new SqlCommand(sql, con);
                        txtStaffName.Text = command.ExecuteScalar().ToString();

                        sql = "SELECT * FROM InvoiceDetail WHERE InvoiceID = " + txtSearch.Text;
                        command = new SqlCommand(sql, con);
                        reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            string ProductID = reader["ProductID"].ToString();
                            sql = "SELECT ProductName FROM Product WHERE ProductID = " + ProductID;
                            command = new SqlCommand(sql, con);
                            string productName = command.ExecuteScalar().ToString();

                            int Quantity = Convert.ToInt32(reader["Quantity"]);
                            double SellingPrice = Convert.ToDouble(reader["SellingPrice"]);
                            double Amount = Convert.ToDouble(reader["Amount"]);

                            string[] row = new string[]
                            {
                                 ProductID,
                                 productName,
                                 Quantity.ToString(),
                                 SellingPrice.ToString("F"),
                                 Amount.ToString("F")
                            };
                            dataGridView1.Rows.Add(row);
                        }
                    dataGridView1.ClearSelection();
                    }
                    con.Close();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
    }
}
