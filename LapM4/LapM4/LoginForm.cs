﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LapM4
{
    public partial class LoginForm : Form
    {
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-FTC9FLG;Initial Catalog=lapM4;Integrated Security=True");
        SqlCommand command;
        string sql;
        public static Int32? LoginAccountID;
        public static string LoginAccountType;
        public static int LoginStaffID;
        public LoginForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Text;
            if(username != " " && password != " ")
            {
                try
                {
                    con.Open();
                    sql = "SELECT * FROM Account WHERE Username = @p1 AND Password = @p2";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue("@p1", txtUsername.Text);
                    command.Parameters.AddWithValue("@p2", txtPassword.Text);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        LoginAccountID = Convert.ToInt32(reader["AccountID"]);
                        LoginAccountType = reader["Type"].ToString();
                        LoginStaffID = Convert.ToInt32(reader["StaffID"]);
                    }
                    reader.Close();
                    con.Close();
                    if(LoginAccountID == null)
                    {
                        MessageBox.Show("Incorrect Username or Password.");
                    }
                    else
                    {
                        new InvoiceForm().Show();
                        this.Hide();
                    }
                }   
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Please fill in Username and Password.");
            }
        }

        private void btclose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private class Show
        {
            public Show()
            {
            }
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                txtPassword.Focus();
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btLogin_Click(this, new EventArgs());
            }
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
