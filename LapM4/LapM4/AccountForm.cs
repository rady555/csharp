﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LapM4
{
    public partial class AccountForm : Form
    {
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-FTC9FLG;Initial Catalog=lapM4;Integrated Security=True");
        SqlCommand command;
        string sql;
        public AccountForm()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void AccountForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'lapM4DataSet7.Account' table. You can move, or remove it, as needed.
            this.accountTableAdapter.Fill(this.lapM4DataSet7.Account);
            if (LoginForm.LoginAccountType == "User")
            {
                btStaffs.Visible = false;
                btAccounts.Visible = false;
            }
            txtUsername.Text = "";
            txtPassword.Text = "";
            txtStaffID.Text = "";
            cbbType.Text = "";
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btSearch_Click(object sender, EventArgs e)
        {
            txtUsername.Text = "";
            txtPassword.Text = "";
            txtStaffID.Text = "";
            cbbType.Text = "";
            if (txtSearch.Text == "")
            {
                MessageBox.Show("Please enter a account ID to search.");
            }
            else
            {
                try
                {
                    con.Open();
                    sql = "SELECT * FROM Account WHERE AccountID = @p1";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue("@p1", txtSearch.Text);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        txtUsername.Text = reader["Username"].ToString();
                        txtPassword.Text = reader["Password"].ToString();
                        cbbType.Text = reader["Type"].ToString();
                        txtStaffID.Text = reader["StaffID"].ToString();
                    }
                    con.Close();

                    if (txtUsername.Text == "")
                    {
                        MessageBox.Show("Search not found");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text == "" || txtPassword.Text == "" || cbbType.Text == "" || txtStaffID.Text == "")
            {
                MessageBox.Show("Please fill in all account information");
            }
            else
            {
                try
                {
                    con.Open();
                    sql = "INSERT INTO Account VALUES(@p1, @p2, @p3, @p4)";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue("@p1", txtUsername.Text);
                    command.Parameters.AddWithValue("@p2", txtPassword.Text);
                    command.Parameters.AddWithValue("@p3", cbbType.Text);
                    command.Parameters.AddWithValue("@p4", txtStaffID.Text);
                    command.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Add successfully.");
                    AccountForm_Load(this, new EventArgs());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == "")
            {
                MessageBox.Show("Please search for customer ID to update.");
            }
            else if (txtUsername.Text == "" || txtPassword.Text == "" || cbbType.Text == "" || txtStaffID.Text == "")
            {
                MessageBox.Show("Please fill in User's name, password, type and staffID.");
            }
            else
            {
                try
                {
                    con.Open();
                    sql = "UPDATE Account SET Username = @p1, Password = @p2," +
                        "Type = @p3, StaffID = @p4 WHERE " +
                        "AccountID = @p5";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue("@p1", txtUsername.Text);
                    command.Parameters.AddWithValue("@p2", txtPassword.Text);
                    command.Parameters.AddWithValue("@p3", cbbType.Text);
                    command.Parameters.AddWithValue("@p4", txtStaffID.Text);
                    command.Parameters.AddWithValue("@p5", txtSearch.Text);
                    command.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Updated successfully.");
                    AccountForm_Load(this, new EventArgs());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == "")
            {
                MessageBox.Show("Please search for account ID to delete.");
            }
            else
            {
                DialogResult dr = MessageBox.Show("Are you sure you want to delete " +
                    "the account ID = " + txtSearch.Text + "?", "", MessageBoxButtons.YesNo);

                if (dr == DialogResult.Yes)
                {
                    try
                    {
                        con.Open();
                        sql = "DELETE FROM Account WHERE AccountID = @p1";
                        command = new SqlCommand(sql, con);
                        command.Parameters.AddWithValue("@p1", txtSearch.Text);
                        int numberOfRowsAffected = command.ExecuteNonQuery();
                        con.Close();

                        if (numberOfRowsAffected == 0)
                        {
                            MessageBox.Show("No account deleted.");
                        }
                        else
                        {
                            MessageBox.Show("Deleted successfully.");
                            AccountForm_Load(this, new EventArgs());

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }

        private void btProducts_Click(object sender, EventArgs e)
        {
            ProductForm pf = new ProductForm();
            pf.Show();
            this.Hide();
        }

        private void btCustomers_Click(object sender, EventArgs e)
        {
            CustomerForm cf = new CustomerForm();
            cf.Show();
            this.Hide();
        }

        private void btInvoices_Click(object sender, EventArgs e)
        {
            InvoiceForm inf = new InvoiceForm();
            inf.Show();
            this.Hide();
        }

        private void btStaffs_Click(object sender, EventArgs e)
        {
            StaffForm sf = new StaffForm();
            sf.Show();
            this.Hide();
        }
    }
}
