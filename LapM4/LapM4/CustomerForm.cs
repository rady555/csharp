﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LapM4
{
    public partial class CustomerForm : Form
    {
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-FTC9FLG;Initial Catalog=lapM4;Integrated Security=True");
        SqlCommand command;
        string sql;
        private object btStaff;
        private object btAccount;

        public CustomerForm()
        {
            InitializeComponent();
        }

        private void CustomerForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'lapM4DataSet4.Customer' table. You can move, or remove it, as needed.
            this.customerTableAdapter.Fill(this.lapM4DataSet4.Customer);
            if (LoginForm.LoginAccountType == "User")
            {
                btStaffs.Visible = false;
                btAccounts.Visible = false;
            }
            txtCustomerName.Text = "";
            txtPhoneNo.Text = "";
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView1.ClearSelection();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btSearch_Click(object sender, EventArgs e)
        {
            txtCustomerName.Text = "";
            txtPhoneNo.Text = "";
            if (txtSearch.Text == "")
            {
                MessageBox.Show("Please enter a customer ID to search.");
            }
            else
            {
                try
                {
                    con.Open();
                    sql = "SELECT * FROM Customer WHERE CustomerID = @p1";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue("@p1", txtSearch.Text);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        txtCustomerName.Text = reader["CustomerName"].ToString();
                        txtPhoneNo.Text = reader["PhoneNo"].ToString();
                    }
                    con.Close();

                    if (txtCustomerName.Text == "")
                    {
                        MessageBox.Show("Search not found");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
        }

        private void btLogout_Click(object sender, EventArgs e)
        {
            LoginForm.LoginAccountID = null;
            LoginForm ss = new LoginForm();
            ss.Show();
            this.Hide();
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            if (txtCustomerName.Text == "" || txtPhoneNo.Text == "")
            {
                MessageBox.Show("Please fill in all customer information");
            }
            else
            {
                try
                {
                    con.Open();
                    sql = "INSERT INTO Customer VALUES(@p1, @p2)";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue("@p1", txtCustomerName.Text);
                    command.Parameters.AddWithValue("@p2", txtPhoneNo.Text);
                    command.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Add successfully.");
                    CustomerForm_Load(this, new EventArgs());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == "")
            {
                MessageBox.Show("Please search for customer ID to update.");
            }
            else if (txtCustomerName.Text == "" || txtPhoneNo.Text == "")
            {
                MessageBox.Show("Please fill in customer's name and phone number.");
            }
            else
            {
                try
                {
                    con.Open();
                    sql = "UPDATE Customer SET CustomerName = @p1, PhoneNo = @p2 WHERE " +
                        "CustomerID = @p3";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue("@p1", txtCustomerName.Text);
                    command.Parameters.AddWithValue("@p2", txtPhoneNo.Text);
                    command.Parameters.AddWithValue("@p3", txtSearch.Text);
                    command.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Updated successfully.");
                    CustomerForm_Load(this, new EventArgs());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == "")
            {
                MessageBox.Show("Please search for custmer ID to delete.");
            }
            else
            {
                DialogResult dr = MessageBox.Show("Are you sure you want to delete " +
                    "the customer ID = " + txtSearch.Text + "?", "", MessageBoxButtons.YesNo);

                if (dr == DialogResult.Yes)
                {
                    try
                    {
                        con.Open();
                        sql = "DELETE FROM Customer WHERE CustomerID = @p1";
                        command = new SqlCommand(sql, con);
                        command.Parameters.AddWithValue("@p1", txtSearch.Text);
                        int numberOfRowsAffected = command.ExecuteNonQuery();
                        con.Close();

                        if (numberOfRowsAffected == 0)
                        {
                            MessageBox.Show("No customer deleted.");
                        }
                        else
                        {
                            MessageBox.Show("Deleted successfully.");
                            CustomerForm_Load(this, new EventArgs());

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }

        private void txtCustomerName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPhoneNo.Focus();
            }
        }

        private void btProducts_Click(object sender, EventArgs e)
        {
            ProductForm pf = new ProductForm();
            pf.Show();
            this.Hide();
        }

        private void btInvoices_Click(object sender, EventArgs e)
        {
            InvoiceForm inf = new InvoiceForm();
            inf.Show();
            this.Hide();
        }

        private void btStaffs_Click(object sender, EventArgs e)
        {
            StaffForm sf = new StaffForm();
            sf.Show();
            this.Hide();
        }

        private void btAccounts_Click(object sender, EventArgs e)
        {
            AccountForm af = new AccountForm();
            af.Show();
            this.Hide();
        }

        private void txtCustomerName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
