﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LapM4
{
    public partial class StaffForm : Form
    {
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-FTC9FLG;Initial Catalog=lapM4;Integrated Security=True");
        SqlCommand command;
        string sql;
        public StaffForm()
        {
            InitializeComponent();
        }

        private void StaffForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'lapM4DataSet3.Staff' table. You can move, or remove it, as needed.
            this.staffTableAdapter.Fill(this.lapM4DataSet3.Staff);
            if (LoginForm.LoginAccountType == "User")
            {
                btStaffs.Visible = false;
                btAccounts.Visible = false;
            }
            txtStaffName.Text = "";
            txtPhoneNo.Text = "";

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView1.ClearSelection();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btSearch_Click(object sender, EventArgs e)
        {
            txtStaffName.Text = "";
            txtPhoneNo.Text = "";
            if (txtSearch.Text == "")
            {
                MessageBox.Show("Please enter a staff ID to search.");
            }
            else
            {
                try
                {
                    con.Open();
                    sql = "SELECT * FROM Staff WHERE StaffID = @p1";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue("@p1", txtSearch.Text);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        txtStaffName.Text = reader["StaffName"].ToString();
                        txtPhoneNo.Text = reader["PhoneNo"].ToString();
                    }
                    con.Close();

                    if (txtStaffName.Text == "")
                    {
                        MessageBox.Show("Search not found");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
        }

        private void btLogOut_Click(object sender, EventArgs e)
        {
            LoginForm.LoginAccountID = null;
            LoginForm ss = new LoginForm();
            ss.Show();
            this.Hide();
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            if (txtStaffName.Text == "" || txtPhoneNo.Text == "")
            {
                MessageBox.Show("Please fill in all staff information");
            }
            else
            {
                try
                {
                    con.Open();
                    sql = "INSERT INTO Staff VALUES(@p1, @p2)";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue("@p1", txtStaffName.Text);
                    command.Parameters.AddWithValue("@p2", txtPhoneNo.Text);
                    command.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Add successfully.");
                    StaffForm_Load(this, new EventArgs());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == "")
            {
                MessageBox.Show("Please search for staff ID to update.");
            }
            else if (txtStaffName.Text == "" || txtPhoneNo.Text == "")
            {
                MessageBox.Show("Please fill in staff's name and phone number.");
            }
            else
            {
                try
                {
                    con.Open();
                    sql = "UPDATE Staff SET StaffName = @p1, PhoneNo = @p2 WHERE " +
                        "StaffID = @p3";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue("@p1", txtStaffName.Text);
                    command.Parameters.AddWithValue("@p2", txtPhoneNo.Text);
                    command.Parameters.AddWithValue("@p3", txtSearch.Text);
                    command.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Updated successfully.");
                    StaffForm_Load(this, new EventArgs());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == "")
            {
                MessageBox.Show("Please search for staff ID to delete.");
            }
            else
            {
                DialogResult dr = MessageBox.Show("Are you sure you want to delete " +
                    "the staff ID = " + txtSearch.Text + "?", "", MessageBoxButtons.YesNo);

                if (dr == DialogResult.Yes)
                {
                    try
                    {
                        con.Open();
                        sql = "DELETE FROM Staff WHERE StaffID = @p1";
                        command = new SqlCommand(sql, con);
                        command.Parameters.AddWithValue("@p1", txtSearch.Text);
                        int numberOfRowsAffected = command.ExecuteNonQuery();
                        con.Close();

                        if (numberOfRowsAffected == 0)
                        {
                            MessageBox.Show("No staff deleted.");
                        }
                        else
                        {
                            MessageBox.Show("Deleted successfully.");
                            StaffForm_Load(this, new EventArgs());

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }

        private void txtStaffName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPhoneNo.Focus();
            }
        }

        private void btProducts_Click(object sender, EventArgs e)
        {
            ProductForm pf = new ProductForm();
            pf.Show();
            this.Hide();
        }

        private void btCustomers_Click(object sender, EventArgs e)
        {
            CustomerForm cf = new CustomerForm();
            cf.Show();
            this.Hide();
        }

        private void btInvoices_Click(object sender, EventArgs e)
        {
            InvoiceForm inf = new InvoiceForm();
            inf.Show();
            this.Hide();
        }

        private void btAccounts_Click(object sender, EventArgs e)
        { 
            AccountForm af = new AccountForm();
            af.Show();
            this.Hide();
        }
    }
}
