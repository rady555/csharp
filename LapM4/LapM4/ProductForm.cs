﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LapM4
{
    public partial class ProductForm : Form
    {
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-FTC9FLG;Initial Catalog=lapM4;Integrated Security=True");
        SqlCommand command;
        string sql;
        private int numberOfRowsAffect;

        public ProductForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'lapM4DataSet6.Product' table. You can move, or remove it, as needed.
            this.productTableAdapter.Fill(this.lapM4DataSet6.Product);
            if (LoginForm.LoginAccountType == "User")
            {
                btStaffs.Visible = false;
                btAccounts.Visible = false;
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView1.ClearSelection();
        }


    

        private void btClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btLogout_Click(object sender, EventArgs e)
        {
            LoginForm.LoginAccountID = null;
            new LoginForm().Show();
            this.Hide();
        }

        private void btSearch_Click(object sender, EventArgs e)
        {
            txtProductName.Text = "";
            txtProductUnitPrice.Text = "";
            if(txtSearch.Text == "")
            {
                MessageBox.Show("Please enter a product ID to search.");
            }
            else
            {
                try
                {
                    con.Open();
                    sql = "SELECT * FROM Product WHERE ProductID = @p1";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue ("@p1", txtSearch.Text);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        txtProductName.Text = reader["ProductName"].ToString();
                        txtProductUnitPrice.Text= reader["UnitPrice"].ToString();
                    }
                    con.Close();

                    if(txtProductName.Text == "")
                    {
                        MessageBox.Show("Search not found");
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {

        }

        private void btSearch_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btSearch_Click(this, new EventArgs());
            }
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            if(txtProductName.Text == "" || txtProductUnitPrice.Text == "")
            {
                MessageBox.Show("Please fill in all Product information");
            }
            else
            {
                try
                {
                    con.Open();
                    sql = "INSERT INTO Product VALUES(@p1, @p2)";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue("@p1", txtProductName.Text);
                    command.Parameters.AddWithValue("@p2", txtProductUnitPrice.Text);
                    command.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Add successfully.");
                    ProductForm_Load(this, new EventArgs());
                }   
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ProductForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'lapM4DataSet9.Product' table. You can move, or remove it, as needed.
            this.productTableAdapter2.Fill(this.lapM4DataSet9.Product);
            if (LoginForm.LoginAccountType == "User")
            {
                btStaffs.Visible = false;
                btAccounts.Visible = false;
            }
            txtProductName.Text = " ";
            txtProductUnitPrice.Text = " ";
        }

        private void txtProductName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtProductUnitPrice.Focus();
            }
        }

        private void txtProductUnitPrice_TextChanged(object sender, EventArgs e)
        {

        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            if(txtSearch.Text == "")
            {
                MessageBox.Show("Please search for product ID to update.");
            }
            else if(txtProductName.Text == "" || txtProductUnitPrice.Text == "")
            {
                MessageBox.Show("Please fill in product's name and unit price.");
            }
            else
            {
                try
                {
                    con.Open();
                    sql = "UPDATE Product SET ProductName = @p1, UnitPrice = @p2 WHERE " +
                        "ProductID = @p3";
                    command = new SqlCommand(sql, con);
                    command.Parameters.AddWithValue("@p1", txtProductName.Text);
                    command.Parameters.AddWithValue("@p2", txtProductUnitPrice.Text);
                    command.Parameters.AddWithValue("@p3", txtSearch.Text);
                    command.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Updated successfully.");
                    ProductForm_Load(this, new EventArgs());
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            if(txtSearch.Text == "")
            {
                MessageBox.Show("Please search for product ID to delete.");
            }
            else
            {
                DialogResult dr = MessageBox.Show("Are you sure you want to delete " +
                    "the product ID = " + txtSearch.Text + "?", "", MessageBoxButtons.YesNo);

                if (dr == DialogResult.Yes)
                {
                    try 
                    {
                        con.Open();
                        sql = "DELETE FROM Product WHERE ProductID = @p1";
                        command = new SqlCommand(sql, con);
                        command.Parameters.AddWithValue("@p1", txtSearch.Text);
                        int numberOfRowsAffected = command.ExecuteNonQuery();
                        con.Close();

                        if(numberOfRowsAffected == 0)
                        {
                            MessageBox.Show("No product deleted.");
                        }
                        else
                        {
                            MessageBox.Show("Deleted successfully.");
                            ProductForm_Load(this, new EventArgs());

                        }
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }

        private void btLogout_Click_1(object sender, EventArgs e)
        {
            LoginForm.LoginAccountID = null;
            LoginForm ss = new LoginForm();
            ss.Show();
            this.Hide();
        }

        private void txtProductName_TextChanged(object sender, EventArgs e)
        {

        }

        private void btCustomers_Click(object sender, EventArgs e)
        {
            CustomerForm cf = new CustomerForm();
            cf.Show();
            this.Hide();
        }

        private void btInvoices_Click(object sender, EventArgs e)
        {
            InvoiceForm Inf = new InvoiceForm();
            Inf.Show();
            this.Hide();
        }

        private void btStaffs_Click(object sender, EventArgs e)
        {
            StaffForm sf = new StaffForm();
            sf.Show();
            this.Hide();
        }

        private void btAccounts_Click(object sender, EventArgs e)
        {
            AccountForm af = new AccountForm();
            af.Show();
            this.Hide();
        }

        private void btProducts_Click(object sender, EventArgs e)
        {

        }
    }
}
