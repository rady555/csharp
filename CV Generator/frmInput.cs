﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
namespace CV_Generator
{
    public partial class frmInput : Form
    {

        private string gender;
        private string tab = "   ";
        private string eng = "";
        private string kh = "";
        private string ch = "";
        private string others = "";

        public frmInput()
        {
            InitializeComponent();
        }

        private enum Style
        {
            center,
            left,
            right
        }

        private void labelStyle(PdfPCell cell, Style type)
        {
            switch(type)
            {
                case Style.center:
                    {
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    }
                    break;
                case Style.left:
                    {
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    }
                    break;
                case Style.right:
                    {
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    }
                    break;
            }
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

        private void chkOther_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOther.Checked)
            {
                txtLanguage.Visible = true;
                others = txtLanguage.Text;
            }
            else
            {
                txtLanguage.Visible = false;
            }
        }
        private void chkEnglish_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEnglish.Checked) { eng = chkEnglish.Text;}
        }

        private void chkKhmer_CheckedChanged(object sender, EventArgs e)
        {
            if (chkKhmer.Checked) { kh = chkKhmer.Text;}

        }

        private void chkChinese_CheckedChanged(object sender, EventArgs e)
        {
            if (chkChinese.Checked) { ch = chkChinese.Text;}
        }
        private void rdoFemale_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoFemale.Checked) { gender = rdoFemale.Text; }
        }
        private void rdoMale_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoMale.Checked) { gender = rdoMale.Text; }
        }

        private bool IsValidata()
        {
            CheckBox[] check = { chkChinese, chkEnglish, chkKhmer, chkOther };
            return Validator.IsValidate(txtName, lblName) &&
                   Validator.IsValidate(txtNationality, lblNationality) &&
                   Validator.IsValidate(txtPhone, lblPhone) &&
                   Validator.IsValidate(txtEmail, lblEmail) &&
                   Validator.IsValidate(txtBornPlace, lblBirthplace) &&
                   Validator.IsValidate(txtAddress, lblAddress) &&
                   Validator.IsValidate(txtPosition, lblPosition) &&
                   Validator.IsValidate(txtQualification, lblQualification) &&
                   Validator.IsValidate(txtSkill, lblSkill) &&
                   Validator.IsValidate(txtExperience, lblExperience) &&
                   Validator.IsSelectRadio(rdoFemale, rdoMale) &&
                   Validator.IsSelectCheck(check); 
                   
        }
        private void btnExport_Click(object sender, EventArgs e)
        {
            if (true)
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "PDF|*.pdf";
                save.FileName = txtName.Text + "'s CV";
                bool errorMessage = false;
                if (save.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(save.FileName))
                    {
                        try
                        {
                            File.Delete(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            errorMessage = true;
                            MessageBox.Show("Unable to write data in disk" + ex.Message);
                        }
                    }
                    if (!errorMessage)
                    {
                        try
                        {
                            Document doc = new Document(PageSize.A4, 16f, 16f, 8f, 8f);

                            BaseFont bFont = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            iTextSharp.text.Font f_26_normal = new iTextSharp.text.Font(bFont, 26, iTextSharp.text.Font.NORMAL);
                            iTextSharp.text.Font f_24_normal = new iTextSharp.text.Font(bFont, 24, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.DARK_GRAY);
                            iTextSharp.text.Font f_14_bold = new iTextSharp.text.Font(bFont, 14, iTextSharp.text.Font.BOLD);
                            iTextSharp.text.Font f_12_bold = new iTextSharp.text.Font(bFont, 12, iTextSharp.text.Font.BOLD);
                            iTextSharp.text.Font f_12_normal = new iTextSharp.text.Font(bFont, 12, iTextSharp.text.Font.NORMAL);
                            iTextSharp.text.Font f_12_underline = new iTextSharp.text.Font(bFont, 12, iTextSharp.text.Font.UNDERLINE);
                       
                            using (FileStream os = new FileStream(save.FileName, FileMode.Create))
                            {
                                PdfWriter writer = PdfWriter.GetInstance(doc, os);
                                doc.Open();

                                PdfPTable table1 = new PdfPTable(1);

                                PdfPCell cell0 = new PdfPCell(new Phrase("CURRICULUM VITAE", f_26_normal));
                                PdfPCell cell1 = new PdfPCell(new Phrase(txtName.Text, f_24_normal));
                                PdfPCell cell2 = new PdfPCell(new Phrase(txtPosition.Text, f_12_bold));
                                
                                labelStyle(cell0, Style.center);
                                labelStyle(cell2, Style.center);
                                labelStyle(cell1, Style.center);

                                cell0.ExtraParagraphSpace = 5;
                                cell1.ExtraParagraphSpace = 5;

                                table1.WidthPercentage = 100;
                                table1.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell0);
                                table1.AddCell(cell1);
                                table1.AddCell(cell2);
                                table1.SpacingBefore = 5;
                                table1.SpacingAfter = 5;
                                doc.Add(table1);

                                PdfPTable tablePerson = new PdfPTable(1);

                                PdfPCell cellPersonInfo = new PdfPCell(new Phrase("PERSONAL INFORMATION", f_14_bold));
                                labelStyle(cellPersonInfo, Style.left);
                                tablePerson.WidthPercentage = 100;
                                tablePerson.HorizontalAlignment = Element.ALIGN_LEFT;
                                tablePerson.AddCell(cellPersonInfo);
                                doc.Add(tablePerson);
                                
                                LineSeparator underline1 = new LineSeparator(1, 100, iTextSharp.text.BaseColor.BLACK, Element.ALIGN_CENTER, -2);
                                doc.Add(new Chunk(underline1));

                                PdfPTable table2 = new PdfPTable(1);

                                string nation = new string(' ', 7);
                                string bd = new string(' ', 4);
                                
                                cell1 = new PdfPCell(new Phrase("Nationality:" + nation + txtNationality.Text, f_12_normal));
                                cell2 = new PdfPCell(new Phrase("Date of birth: " + bd + txtBirthdate.Text, f_12_normal));
                                PdfPCell cell3 = new PdfPCell(new Phrase("Place of birth: " + tab + txtBornPlace.Text, f_12_normal));
                                PdfPCell cell4 = new PdfPCell(new Phrase("Phone: " + tab + txtPhone.Text, f_12_normal));
                                PdfPCell cell5 = new PdfPCell(new Phrase("Email: "+ tab + txtEmail.Text, f_12_normal));
                                PdfPCell cell6 = new PdfPCell(new Phrase("Gender: " + tab + gender, f_12_normal));
                                PdfPCell cell7 = new PdfPCell(new Phrase("Address: " + tab + txtAddress.Text, f_12_normal));
                                
                                labelStyle(cell1, Style.left);
                                labelStyle(cell2, Style.left);
                                labelStyle(cell3, Style.left);
                                labelStyle(cell4, Style.left);
                                labelStyle(cell5, Style.left);
                                labelStyle(cell6, Style.left);
                                labelStyle(cell7, Style.left);

                                table2.WidthPercentage = 100;
                                table2.HorizontalAlignment = Element.ALIGN_LEFT;
                                table2.AddCell(cell1);
                                table2.AddCell(cell2);
                                table2.AddCell(cell3);
                                table2.AddCell(cell4);
                                table2.AddCell(cell5);
                                table2.AddCell(cell6);
                                table2.AddCell(cell7);                     
                                table2.SpacingBefore = 5;
                                table2.SpacingAfter = 5;
                                doc.Add(table2);

                                PdfPTable tableQualification = new PdfPTable(1);

                                PdfPCell cellQualification = new PdfPCell(new Phrase("QUANLIFICATION", f_14_bold));
                                labelStyle(cellQualification, Style.left);
                                tableQualification.WidthPercentage = 100;
                                tableQualification.HorizontalAlignment = Element.ALIGN_LEFT;
                                tableQualification.AddCell(cellQualification);
                                doc.Add(tableQualification);

                                LineSeparator underline2 = new LineSeparator(1, 100, iTextSharp.text.BaseColor.BLACK, Element.ALIGN_CENTER, -2);
                                doc.Add(new Chunk(underline2));

                                Paragraph quanlification = new Paragraph(new Phrase(txtQualification.Text, f_12_normal));
                                doc.Add(quanlification);

                                PdfPTable tableLanguage = new PdfPTable(1);

                                PdfPCell cellLanguage = new PdfPCell(new Phrase("LANGUAGE", f_14_bold));
                                labelStyle(cellLanguage, Style.left);

                                tableLanguage.WidthPercentage = 100;
                                tableLanguage.HorizontalAlignment = Element.ALIGN_LEFT;
                                tableLanguage.AddCell(cellLanguage);
                                tableLanguage.SpacingBefore = 5;
                                tableLanguage.SpacingAfter = -30;
                                doc.Add(tableLanguage);

                                LineSeparator underline3 = new LineSeparator(1, 100, iTextSharp.text.BaseColor.BLACK, Element.ALIGN_CENTER, -14);
                                doc.Add(new Chunk(underline3));

                                PdfPTable table4 = new PdfPTable(1);
                               
                                cell1 = new PdfPCell(new Phrase(eng, f_12_normal));
                                cell2 = new PdfPCell(new Phrase(kh, f_12_normal));
                                cell3 = new PdfPCell(new Phrase(ch, f_12_normal));
                                cell4 = new PdfPCell(new Phrase(txtLanguage.Text, f_12_normal));

                                labelStyle(cell1, Style.left);
                                labelStyle(cell2, Style.left);
                                labelStyle(cell3, Style.left);
                                labelStyle(cell4, Style.left);

                                table4.WidthPercentage = 100;
                                table4.HorizontalAlignment = Element.ALIGN_LEFT;
                                table4.AddCell(cell1);
                                table4.AddCell(cell2);
                                table4.AddCell(cell3);
                                table4.AddCell(cell4);
                                doc.Add(table4);

                                PdfPTable tableWork = new PdfPTable(1);
                                cell0 = new PdfPCell(new Phrase("WORK EXPERIENCE", f_14_bold));
                                labelStyle(cell0, Style.left);
                                tableWork.WidthPercentage = 100;
                                tableWork.HorizontalAlignment = Element.ALIGN_LEFT;
                                tableWork.SpacingAfter = -17;
                                tableWork.AddCell(cell0);
                                doc.Add(tableWork);

                                LineSeparator underline4 = new LineSeparator(1, 100, iTextSharp.text.BaseColor.BLACK, Element.ALIGN_CENTER, -1);
                                doc.Add(new Chunk(underline4));

                                Paragraph work = new Paragraph(new Phrase(txtExperience.Text, f_12_normal));
                                doc.Add(work);

                                PdfPTable tableSkill = new PdfPTable(1);
                                cell0 = new PdfPCell(new Phrase("SKILLS", f_14_bold));
                                labelStyle(cell0, Style.left);
                                tableSkill.WidthPercentage = 100;
                                tableSkill.HorizontalAlignment = Element.ALIGN_LEFT;
                                tableSkill.SpacingBefore = 5;
                                tableSkill.SpacingAfter = -17;
                                tableSkill.AddCell(cell0);
                                doc.Add(tableSkill);

                                LineSeparator underline5 = new LineSeparator(1, 100, iTextSharp.text.BaseColor.BLACK, Element.ALIGN_CENTER, -1);
                                doc.Add(new Chunk(underline5));

                                Paragraph skill = new Paragraph(new Phrase(txtSkill.Text, f_12_normal));
                                doc.Add(skill);
                                doc.Close();
                                os.Close();
                                System.Diagnostics.Process.Start(@save.FileName);
                            }
                            MessageBox.Show("Data export successfully.", "Info");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error while export data!" + ex.Message);
                        }
                    }
                }
            }
        }

        private void txtName_TextChanged(object sender, PaintEventArgs e)
        {
           
        }

        private void frmInput_Load(object sender, EventArgs e)
        {

        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            lblName.Visible = false;
        }

        private void txtNationality_TextChanged(object sender, EventArgs e)
        {
            lblNationality.Visible = false;
        }

        private void txtPhone_TextChanged(object sender, EventArgs e)
        {
            lblPhone.Visible = false;
        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {
            lblEmail.Visible = false;
        }

        private void txtBornPlace_TextChanged(object sender, EventArgs e)
        {
            lblBirthplace.Visible = false;
        }

        private void txtAddress_TextChanged(object sender, EventArgs e)
        {
            lblAddress.Visible = false;
        }

        private void txtSkill_TextChanged(object sender, EventArgs e)
        {
            lblSkill.Visible = false;
        }

        private void txtExperience_TextChanged(object sender, EventArgs e)
        {
            lblExperience.Visible = false;
        }
        private void txtQualification_TextChanged(object sender, EventArgs e)
        {
            lblQualification.Visible = false;
        }

        private void txtPosition_TextChanged(object sender, EventArgs e)
        {
            lblPosition.Visible = false;
        }

        private void lblNationality_Click(object sender, EventArgs e)
        {

        }

        private void lblPhone_Click(object sender, EventArgs e)
        {

        }
    }
}
