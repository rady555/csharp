﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace CV_Generator
{
    public static class Validator
    {
        private static string title = "Entry Error";
        public static string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }
        public static bool IsSelectRadio(RadioButton radioButton1, RadioButton radioButton2)
        {
            if (radioButton1.Checked == false && radioButton2.Checked == false)
            {
                MessageBox.Show("You must select your gender!", Title);
                return false;
            }
            return true;
        }
        public static bool IsSelectCheck(CheckBox[] checkBoxes)
        {
            if (checkBoxes[0].Checked == false && checkBoxes[1].Checked == false && checkBoxes[2].Checked == false && checkBoxes[3].Checked == false)
            {
                MessageBox.Show("You must select language", Title);
                return false;
            }
            return true;
        }
        public static bool IsValidate(TextBox textbox, Label label)
        {
            if(textbox.Text == "")
            {
                label.Visible = true;
                textbox.Focus();
                return false;
            }   
            return true;
        }

    }
}